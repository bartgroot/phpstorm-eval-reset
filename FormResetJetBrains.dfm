object frmResetJetBrains: TfrmResetJetBrains
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'JetBrains evaluation period resetter'
  ClientHeight = 329
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 41
    Width = 635
    Height = 288
    Align = alClient
    Lines.Strings = (
      
        'Please close all running instances of the selected application a' +
        'nd click the '#39'Reset evaluation period'#39' button above to continue')
    ReadOnly = True
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      635
      41)
    object Label1: TLabel
      Left = 8
      Top = 14
      Width = 87
      Height = 13
      Caption = 'Select application:'
    end
    object btnReset: TButton
      Left = 256
      Top = 8
      Width = 153
      Height = 25
      Caption = 'Reset evaluation period'
      Default = True
      TabOrder = 1
      OnClick = btnResetClick
    end
    object btnExit: TButton
      Left = 552
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Exit'
      TabOrder = 2
      OnClick = btnExitClick
    end
    object cmbApp: TComboBox
      Left = 101
      Top = 10
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 0
      Text = 'PhpStorm'
      Items.Strings = (
        'PhpStorm'
        'AppCode'
        'CLion'
        'DataGrip'
        'GoLand'
        'IntelliJ'
        'PyCharm'
        'Resharper'
        'Rider'
        'RubyMine'
        'WebStorm')
    end
  end
end
