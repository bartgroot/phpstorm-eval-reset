program ResetJetBrainsEval;

uses
  Vcl.Forms,
  FormResetJetBrains in 'FormResetJetBrains.pas' {frmResetJetBrains},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Carbon');
  Application.CreateForm(TfrmResetJetBrains, frmResetJetBrains);
  Application.Run;
end.
