unit FormResetJetBrains;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TfrmResetJetBrains = class(TForm)
    btnReset: TButton;
    Memo1: TMemo;
    btnExit: TButton;
    Panel1: TPanel;
    cmbApp: TComboBox;
    Label1: TLabel;
    procedure btnResetClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmResetJetBrains: TfrmResetJetBrains;

implementation

{$R *.dfm}

uses System.IOUtils, Registry;

procedure TfrmResetJetBrains.btnResetClick(Sender: TObject);
var
  arDirs, arFiles: TArray<String>;
begin
  var bTestMode := False;
  var sApp: string := cmbApp.Text;
  Memo1.Lines.Add('Detecting...');

  var arDirs1 := TDirectory.GetDirectories(GetEnvironmentVariable('USERPROFILE'), '.'+sApp+'*'); //old (before 2020 version)
  var arDirs2 := TDirectory.GetDirectories(GetEnvironmentVariable('USERPROFILE')+'\AppData\Roaming\JetBrains', sApp+'*');
  arDirs := Concat(arDirs1,arDirs2);
  for var sDir in arDirs do begin
    // Step 1: del %USERPROFILE%\.PhpStorm2019.1\config\eval\*.key
    arFiles := TDirectory.getFiles(sDir, '*.key', TSearchOption.soAllDirectories);
    for var sFile in arFiles do begin
      Memo1.Lines.Add('Deleting '+sFile);
      if not bTestMode then
        TFile.Delete(sFile);
    end;

    // Step 2: %USERPROFILE%\.PhpStorm2019.1\config\options\other.xml: Delete all lines witht "eval"/"evl" from xml
    arFiles := TDirectory.getFiles(sDir, 'other.xml', TSearchOption.soAllDirectories);
    for var sFile in arFiles do begin
      Memo1.Lines.Add('Checking '+sFile);
      var slFile := TStringlist.Create;
      try
        slFile.LoadFromFile(sFile);
        var bFileModified := False;
        for var i := slFile.Count-1 downto 0 do begin
          if slFile[i].Contains('"evlsprt') then begin
            slFile.Delete(i);
            bFileModified := True;
          end;
        end;
        if bFileModified then begin
          if not bTestMode then
            slFile.SaveToFile(sFile);
          Memo1.Lines.Add('Saved '+sFile);
        end
        else Memo1.Lines.Add('No changes to '+sFile);
      finally
        slFile.Free;
      end;
    end;
  end;

  //Step 3: registry remove all keys begining with evlsprt from HKEY_CURRENT_USER\Software\JavaSoft\Prefs\jetbrains\phpstorm\
  var reg := TRegistry.Create;
  try
    reg.RootKey := HKEY_CURRENT_USER;
    var sBaseKey := '\Software\JavaSoft\Prefs\jetbrains\'+sApp.ToLower;
    reg.OpenKey(sBaseKey, False);
    var slKeys := TStringlist.Create;
    try
      reg.GetKeyNames(slKeys);
      for var sKey in slKeys do begin
        reg.OpenKey(sBaseKey+'\'+sKey, False);
        var slSubKeys := TStringlist.Create;
        try
          reg.GetKeyNames(slSubKeys);
          for var sSubkey in slSubKeys do begin
            if sSubkey.StartsWith('evlsprt') then begin
              Memo1.Lines.Add('Deleting from registry: '+reg.CurrentPath + '\' +sSubkey);
              if not bTestMode then begin
                reg.DeleteKey(sSubKey);
              end;
            end;
          end;
        finally
          slSubKeys.Free;
        end;
      end;
    finally
      slKeys.Free;
    end;
  finally
    reg.Free;
  end;
  Memo1.Lines.Add('Ready. You can now run '+sApp+' and start a new evaluation period!');
end;

procedure TfrmResetJetBrains.btnExitClick(Sender: TObject);
begin
  Close;
end;

end.
